let num = 75.1

if(num >= 0 && num <= 25){
    console.log("O número está no intervalo [0,25]");
}
else if(num > 25 && num <= 50){
    console.log("O número está no intervalo (25,50]");
}
else if(num > 50 && num <= 75){
    console.log("O número está no intervalo (50,75]");
}
else if(num > 75 && num <= 100){
    console.log("O número está no intervalo (75,100]");
}
else{
    console.log("Fora de intervalo");
}